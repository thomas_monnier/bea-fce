import subprocess

def source(input, output):
    cmd = [f"""grep ^S {input} | cut -c 3- > {output+'_source.txt'}"""]
    subprocess.check_output(cmd, shell=True)

# Apply the edits of a single annotator to generate the corrected sentences.
def reference(input, output):
	m2 = open(input).read().strip().split("\n\n")
	out = open(output+'_reference.txt', "w")
	# Do not apply edits with these error types
	skip = {"noop", "UNK", "Um"}
	
	for sent in m2:
		sent = sent.split("\n")
		cor_sent = sent[0].split()[1:] # Ignore "S "
		edits = sent[1:]
		offset = 0
		for edit in edits:
			edit = edit.split("|||")
			if edit[1] in skip: continue # Ignore certain edits
			coder = int(edit[-1])
			if coder != 0: continue # Ignore other coders
			span = edit[0].split()[1:] # Ignore "A "
			start = int(span[0])
			end = int(span[1])
			cor = edit[2].split()
			cor_sent[start+offset:end+offset] = cor
			offset = offset-(end-start)+len(cor)
		out.write(" ".join(cor_sent)+"\n")

def error_categories(input, output):
	m2 = open(input).read().strip().split("\n\n")
	out = open(output+'_categories.txt', "w")
	# Do not apply edits with these error types
	skip = {"noop", "UNK", "Um"}

	for sent in m2:
		sent = sent.split("\n")
		cor_sent = []
		edits = sent[1:]
		offset = 0
		for edit in edits:
			edit = edit.split("|||")
			'''if edit[1] in skip:
				cor_sent
				continue # Ignore certain edits'''
			coder = int(edit[-1])
			if coder != 0: continue # Ignore other coders
			span = edit[0].split()[1:] # Ignore "A "
			start = int(span[0])
			end = int(span[1])
			cor = edit[1].split()
			cor_sent[start+offset:end+offset] = cor
			offset = offset-(end-start)+len(cor)

		out.write(" ".join(cor_sent)+"\n")

def main(set):
    source(set['input'], set['output'])
    reference(set['input'], set['output'])
    error_categories(set['input'], set['output'])

if __name__ == "__main__":
    bea_train = {'input': 'source/bea/ABC.train.gold.bea19.m2', 'output': '0_parallel_source/bea/bea_train'}
    bea_dev = {'input': 'source/bea/ABCN.dev.gold.bea19.m2', 'output': '0_parallel_source/bea/bea_dev'}
    fce_train = {'input': 'source/fce/fce.train.gold.bea19.m2', 'output': '0_parallel_source/fce/fce_train'}
    fce_dev = {'input': 'source/fce/fce.dev.gold.bea19.m2', 'output': '0_parallel_source/fce/fce_dev'}
    fce_test = {'input': 'source/fce/fce.test.gold.bea19.m2', 'output': '0_parallel_source/fce/fce_test'}

    sets = [bea_train, bea_dev, fce_train, fce_dev, fce_test]
    for set in sets:
        main(set)