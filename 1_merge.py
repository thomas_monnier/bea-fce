import pandas as pd
import argparse

def get_lines(file):
    with open(file, 'r') as f:
        lines = f.readlines()
        f.close()
    for i, line in enumerate(lines):
        lines[i] = line[:-1]
    return lines

def save_csv(df, output):
    return df.to_csv(output, index=False)

def fill_csv(set):
    lines_source = get_lines(set['source'])
    lines_reference = get_lines(set['reference'])
    lines_categories = get_lines(set['categories'])

    matrix = [[a, b, c] for a, b, c in zip(lines_source, lines_reference, lines_categories)]
    df = pd.DataFrame(matrix, columns=['Original sentence', 'Expected sentence', 'Mistake categories'])
    df = df[df['Expected sentence'] != '']
    return df

def main(set):
    df_test = fill_csv(set)
    save_csv(df_test, set['output'])

if __name__ == "__main__":
    bea_train = {'source': '0_parallel_source/bea/bea_train_source.txt', 'reference': '0_parallel_source/bea/bea_train_reference.txt', 'categories': '0_parallel_source/bea/bea_train_categories.txt', 'output': '1_set/bea/bea_train.csv'}
    bea_dev = {'source': '0_parallel_source/bea/bea_dev_source.txt', 'reference': '0_parallel_source/bea/bea_dev_reference.txt', 'categories': '0_parallel_source/bea/bea_dev_categories.txt', 'output': '1_set/bea/bea_dev.csv'}
    fce_train = {'source': '0_parallel_source/fce/fce_train_source.txt', 'reference': '0_parallel_source/fce/fce_train_reference.txt', 'categories': '0_parallel_source/fce/fce_train_categories.txt', 'output': '1_set/fce/fce_train.csv'}
    fce_dev = {'source': '0_parallel_source/fce/fce_dev_source.txt', 'reference': '0_parallel_source/fce/fce_dev_reference.txt', 'categories': '0_parallel_source/fce/fce_dev_categories.txt', 'output': '1_set/fce/fce_dev.csv'}
    fce_test = {'source': '0_parallel_source/fce/fce_test_source.txt', 'reference': '0_parallel_source/fce/fce_test_reference.txt', 'categories': '0_parallel_source/fce/fce_test_categories.txt', 'output': '1_set/fce/fce_test.csv'}

    sets = [bea_train, bea_dev, fce_train, fce_dev, fce_test]
    for set in sets:
        main(set)