import pandas as pd
import copy
import subprocess
import importlib
import os

lib_0 = importlib.import_module('0_m2_to_parallel', package=None)
lib_1 = importlib.import_module('1_merge',          package=None)
lib_2 = importlib.import_module('2_detokenize',     package=None)

def create_full_set_m2():
    cmd = ["cat source/bea/* source/fce/* > 6_intermediary_files/full_set.m2"]
    subprocess.check_output(cmd, shell=True)

def save_m2(sentences, message, category):
    category = category.replace(':', '-')
    with open(f'6_intermediary_files/full_set_{category}_{message}', 'w') as f:
        for sentence in sentences:
            f.write(sentence)
        f.close()

def process_sentence(sentence, category):
    annotations = []
    for line in sentence[1:]:
        data = line[:-1].split('|||')
        if category in data[1] and f'{category}:' not in data[1]:
            annotations.append(data[-1])
            sentence.remove(line)
    return sentence, annotations

def keep_one_category(category):
    with open('6_intermediary_files/full_set.m2', 'r') as f:
        #lines = [line.replace('\n', '') for line in f.readlines()]
        lines = f.readlines()
        sentences = [[]]
        processed_sentences = []
        original_sentences = []
        for i, line in enumerate(lines):
            if line == '\n' or i == len(lines)-1:
                original_sentence = copy.copy(sentences[-1])
                sentence_to_process = sentences.pop()
                processed_sentence, annotations = process_sentence(sentence_to_process, category)
                if annotations:
                    processed_sentences += processed_sentence
                    original_sentences += original_sentence
                    processed_sentences.append('\n')
                    original_sentences.append('\n')
                sentences.append([])
            else:
                sentences[0].append(line)

        save_m2(processed_sentences, 'without', category)
        save_m2(original_sentences, 'with', category)
        f.close()

def convert(category_file, message):
    lib_0.reference(f'6_intermediary_files/full_set_{category_file}_{message}', f'6_intermediary_files/full_set_{category_file}_{message}')
    os.remove(f'6_intermediary_files/full_set_{category_file}_{message}')
    lines_without = lib_1.get_lines(f'6_intermediary_files/full_set_{category_file}_{message}_reference.txt')
    os.remove(f'6_intermediary_files/full_set_{category_file}_{message}_reference.txt')
    return lines_without

def extract(category, df, type):
    category_types = ["F:"+category, "M:"+category, "R:"+category, "U:"+category, "D:"+category]
    if type == 'every':
        index = []
        for iter, row in df.iterrows():
            categories = row[2].split()
            s = False
            for cat in categories:
                if cat in category_types:
                    s = True
            if s:
                index.append(iter)
        df = df.loc[index]
        category = category.replace(':', '-')
        df.to_csv('4_every/'+category+'.csv', index=False)
    
    elif type == 'only':
        df = df[df['Mistake categories'].isin(category_types)]
        category = category.replace(':', '-')
        df.to_csv('5_only/'+category+'.csv', index=False)
    
    elif type == 'only_all':
        keep_one_category(category)
        category_file = category.replace(':', '-')
        lines_with = convert(category_file, 'with')
        lines_without = convert(category_file, 'without')

        matrix = [[a, b] for a, b in zip(lines_without, lines_with)]
        df = pd.DataFrame(matrix, columns=['Original sentence', 'Expected sentence'])
        df = df[df['Expected sentence'] != '']
        lib_1.save_csv(df, f'6_intermediary_files/full_set_{category_file}.csv')   

        lib_2.main({"input": f'6_intermediary_files/full_set_{category_file}.csv', "output": f'6_only_all/{category_file}.csv'})
        os.remove(f'6_intermediary_files/full_set_{category_file}.csv')

if __name__ == "__main__":
    create_full_set_m2()
    error_categories = pd.read_csv("error_categories.csv")
    detokenized_full_set = pd.read_csv("3_full_set/bea_fce.csv")
    for value in error_categories.Category:
        print("Category: ", value)
        extract(value, detokenized_full_set, 'every')
        extract(value, detokenized_full_set, 'only')
        extract(value, detokenized_full_set, 'only_all')
        print("Done")
        print("#######################")