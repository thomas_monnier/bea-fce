import pandas as pd
import importlib 

lib_1 = importlib.import_module("1_merge", package=None)

def extract_well_written(df):
    return df[df['Mistake categories'] == 'noop'].iloc[:, :2]

if __name__=="__main__":
    df = pd.read_csv("3_full_set/bea_fce.csv")
    df = extract_well_written(df)
    lib_1.save_csv(df, "7_correct_sentences/correct.csv")