import pandas as pd
from nltk.tokenize.treebank import TreebankWordDetokenizer

def rules(sentence):
  """
  This function detokenizes quotes and hyphens.
  Input: str
  Example: 'I told you : "Why do you think I'm a ten-year-old guy ?"
  """
  return sentence.replace(' ?', '?').replace(' !', '!').replace(' ,', ',').replace(' .', '.').replace(' :', ':').replace(' ;', ';')

def nltk_detokenization(sentence):
  """
  This functions detokenizes all ponctuation marks, including paranthesis.
  Input: bag of words. 
  Example: ["I", "'ve", "many", "dogs", "."]
  """
  return TreebankWordDetokenizer().detokenize(sentence)

def detokenize(df):
  for _, row in df.iterrows():
    for i in range(2):
      row[i] = rules(row[i])
      original = nltk_detokenization(row[i].split())
      row[i] = original
  return df

def save_csv(df, output):
  return df.to_csv(output, index=False)

def main(set):
  df = pd.read_csv(set['input'])
  df = detokenize(df)
  save_csv(df, set['output'])

if __name__ == "__main__":
  bea_train = {'input': '1_set/bea/bea_train.csv', 'output': '2_detokenized_set/bea/bea_train.csv'}
  bea_dev = {'input': '1_set/bea/bea_dev.csv', 'output': '2_detokenized_set/bea/bea_dev.csv'}
  fce_train = {'input': '1_set/fce/fce_train.csv', 'output': '2_detokenized_set/fce/fce_train.csv'}
  fce_dev = {'input': '1_set/fce/fce_dev.csv', 'output': '2_detokenized_set/fce/fce_dev.csv'}
  fce_test = {'input': '1_set/fce/fce_test.csv', 'output': '2_detokenized_set/fce/fce_test.csv'}

  sets = [bea_train, bea_dev, fce_train, fce_dev, fce_test]
  for set in sets:
    main(set)