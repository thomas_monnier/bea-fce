import subprocess
import pandas as pd

def assemble(files):
    df = pd.concat(map(pd.read_csv, [file for file in files]), ignore_index=True)
    return df

def save_csv(df, output):
    return df.to_csv(output, index=False)

if __name__ == "__main__":
    inputs = ['2_detokenized_set/bea/bea_train.csv', 
        '2_detokenized_set/bea/bea_dev.csv', 
        '2_detokenized_set/fce/fce_train.csv', 
        '2_detokenized_set/fce/fce_dev.csv', 
        '2_detokenized_set/fce/fce_test.csv']
    output = '3_full_set/bea_fce.csv'

    df = assemble(inputs)
    save_csv(df, output)